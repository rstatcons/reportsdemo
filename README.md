# R-package for demonstration of possibilites for report producing

## Package content

1. [Slides](vignettes/reports_in_r_slides.Rpres) (in Russian).
1. [Script](inst/extdata/getsourcedata.R) for economical data downloading and processing.
1. [Script](inst/extdata/getshapefiles.R) for retreiving and preparing of shape data for Kazakhstan's boundaries.
1. Prepared economical [data](data/bizstat.rdata).
1. Prepared shape [data](data/shapes.rdata).
