#' Statistics of companies in Kazakhstan by size and by region.
#'
#' March 2016. From www.stat.gov.kz.
"bizstat"

#' Shape file for KZ
#'
#' From gis-lab.info project.
"shapes"

#' Ethnic groups, age, gender, and area by regions of KZ
#'
#' List with four data frames. Regions' and ethnic groups' names
#' are given in Russian. From www.stat.gov.kz.
#' Area is in thousands of square km.
"kzpopulation"
